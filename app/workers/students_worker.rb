# frozen_string_literal: true

class StudentsWorker
  include Sidekiq::Worker

  def perform(xlsx_data)
    CreateStudents.new(xlsx_data).load_data
  end
end
