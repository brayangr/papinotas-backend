# frozen_string_literal: true

class CoursesWorker
  include Sidekiq::Worker

  def perform(xlsx_data)
    CreateCourses.new(xlsx_data).load_data
  end
end
