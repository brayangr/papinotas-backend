# frozen_string_literal: true

class PeriodsWorker
  include Sidekiq::Worker

  def perform(xlsx_data)
    CreatePeriods.new(xlsx_data).load_data
  end
end
