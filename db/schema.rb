# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2019_07_07_213130) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "course_period_students", force: :cascade do |t|
    t.bigint "course_id"
    t.bigint "period_id"
    t.bigint "student_id"
    t.integer "list_number"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["course_id", "list_number", "period_id"], name: "course_list_number_period", unique: true
    t.index ["course_id", "period_id", "student_id"], name: "course_period_student", unique: true
    t.index ["course_id"], name: "index_course_period_students_on_course_id"
    t.index ["period_id"], name: "index_course_period_students_on_period_id"
    t.index ["student_id"], name: "index_course_period_students_on_student_id"
  end

  create_table "courses", force: :cascade do |t|
    t.string "full_name"
    t.string "short_name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["full_name"], name: "index_courses_on_full_name", unique: true
    t.index ["short_name"], name: "index_courses_on_short_name", unique: true
  end

  create_table "periods", force: :cascade do |t|
    t.string "name"
    t.date "start_date"
    t.date "end_date"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name"], name: "index_periods_on_name", unique: true
  end

  create_table "students", force: :cascade do |t|
    t.string "name"
    t.string "paternal_surname"
    t.string "maternal_surname"
    t.string "rut"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["rut"], name: "index_students_on_rut", unique: true
  end

  add_foreign_key "course_period_students", "courses"
  add_foreign_key "course_period_students", "periods"
  add_foreign_key "course_period_students", "students"
end
