# frozen_string_literal: true
class CreateCourses
  require 'roo'

  def initialize(xlsx_data)
    @xlsx_route = 'courses.xlsx'
    decode_base64_content = Base64.decode64(xlsx_data)
    File.open(@xlsx_route, 'wb') do |f|
      f.write(decode_base64_content)
    end
  end

  def load_data
    xlsx = Roo::Spreadsheet.open(@xlsx_route)

    ActiveRecord::Base.transaction do
      xlsx.each(full_name: 'Nombre', short_name: 'Nombre corto') do |hash|
        next if hash[:full_name] == 'Nombre' && hash[:short_name] == 'Nombre corto'

        course = Course.new(
          full_name: hash[:full_name],
          short_name: hash[:short_name]
        )

        next if course.save

        message = { error: course.errors.full_messages, id: hash[:full_name] }
        ActionCable.server.broadcast 'messages_channel', message

        return
      end
    end

    message = { success: 'Datos cargados correctamente' }
    ActionCable.server.broadcast 'messages_channel', message

    File.delete(@xlsx_route) if File.exist?(@xlsx_route)
  end
end
