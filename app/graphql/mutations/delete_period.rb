# frozen_string_literal: true

class Mutations::DeletePeriod < Mutations::BaseMutation
  argument :id, ID, required: true

  field :period, Types::PeriodType, null: true
  field :errors, [String], null: false

  def resolve(id:)
    period = Period.where(id: id).first

    if !period.nil? && period.course_period_students.empty? && period.delete
      { period: period, errors: [] }
    elsif period.nil?
      { period: nil, errors: ["period with id: #{id} not found"]}
    else
      { period: nil, errors: ["period with id: #{id} can't be deleted"] }
    end
  end
end
