# frozen_string_literal: true

# == Schema Information
#
# Table name: course_period_students
#
#  id          :bigint           not null, primary key
#  course_id   :bigint
#  period_id   :bigint
#  student_id  :bigint
#  list_number :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
class CoursePeriodStudent < ApplicationRecord
  belongs_to :course
  belongs_to :period
  belongs_to :student

  validates :list_number, presence: true, numericality: { only_integer: true }
  validates :list_number, uniqueness: { scope: %i[course period] }
  validates :student, uniqueness: { scope: %i[course period] }
end
