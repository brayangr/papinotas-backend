FROM ruby:2.6.3

RUN mkdir /papinotas-backend
WORKDIR /papinotas-backend

COPY Gemfile /papinotas-backend/Gemfile
COPY Gemfile.lock /papinotas-backend/Gemfile.lock

RUN bundle install
