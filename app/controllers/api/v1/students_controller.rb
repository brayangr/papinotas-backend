# frozen_string_literal: true

class Api::V1::StudentsController < ApplicationController
  def index
    path = './students.xlsx'

    generate_file(path)

    send_file(
      path,
      filename: 'nomina_estudiantes.xlsx',
      disposition: 'inline',
      type: 'xlsx'
    )
  end

  def generate_file(path)
    workbook = FastExcel.open
    worksheet = workbook.add_worksheet

    headers = [
      'Nombre',
      'Apellido Paterno',
      'Apellido Materno',
      'Rut',
      'Curso',
      'Periodo',
      'Número de lista'
    ]

    worksheet.append_row(headers)

    Student.eager_load(:courses).each do |student|
      row_data = [
        student.name,
        student.paternal_surname,
        student.maternal_surname,
        student.rut,
        student.courses&.first&.course&.short_name,
        student.courses&.first&.period&.name,
        student.courses&.first&.list_number
      ]

      worksheet.append_row(row_data)
    end

    content = workbook.read_string
    File.open(path, 'wb') {|f| f.write(content) }
  end
end
