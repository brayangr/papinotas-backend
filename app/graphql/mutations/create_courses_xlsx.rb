# frozen_string_literal: true

class Mutations::CreateCoursesXlsx < Mutations::BaseMutation
  argument :file, ApolloUploadServer::Upload, required: false

  field :errors, [String], null: false

  def resolve(file:)
    CoursesWorker.perform_async(Base64.encode64(File.read(file.tempfile)))
    { errors: [] }
  end
end
