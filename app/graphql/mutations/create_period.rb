# frozen_string_literal: true

class Mutations::CreatePeriod < Mutations::BaseMutation
  argument :name, String, required: true
  argument :start_date, String, required: true
  argument :end_date, String, required: true

  field :period, Types::PeriodType, null: true
  field :errors, [String], null: false

  def resolve(name:, start_date:, end_date:)
    period = Period.new(name: name, start_date: start_date, end_date: end_date)

    if period.save
      { period: period, errors: [] }
    else
      { period: nil, errors: period.errors.full_messages }
    end
  end
end
