class CreatePeriods < ActiveRecord::Migration[5.2]
  def change
    create_table :periods do |t|
      t.string :name
      t.date :start_date
      t.date :end_date

      t.index :name, unique: true

      t.timestamps
    end
  end
end
