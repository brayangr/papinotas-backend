class CreateCourses < ActiveRecord::Migration[5.2]
  def change
    create_table :courses do |t|
      t.string :full_name
      t.string :short_name

      t.index :full_name, unique: true
      t.index :short_name, unique: true

      t.timestamps
    end
  end
end
