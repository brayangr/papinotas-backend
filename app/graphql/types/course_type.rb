# frozen_string_literal:true

module Types
  class CourseType < Types::BaseObject
    field :id, ID, null: false
    field :full_name, String, null: false
    field :short_name, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
