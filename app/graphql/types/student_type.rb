# frozen_string_literal:true

module Types
  class StudentType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :paternal_surname, String, null: false
    field :maternal_surname, String, null: false
    field :rut, String, null: false
    field :courses, CoursePeriodStudentType.connection_type, null: true
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
