# frozen_string_literal: true

module Types
  class CoursePeriodStudentType < Types::BaseObject
    field :id, ID, null: false
    field :course, Types::CourseType, null: false
    field :period, Types::PeriodType, null: false
    field :student, Types::StudentType, null: false
    field :list_number, Integer, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
