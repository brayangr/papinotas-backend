# frozen_string_literal: true

module Types
  class QueryType < Types::BaseObject
    field :courses, [Types::CourseType], null: false

    def courses
      Course.all
    end

    field :periods, [Types::PeriodType], null: false

    def periods
      Period.all
    end

    field :active_periods, [Types::PeriodType], null: false

    def active_periods
      Period.active
    end

    field :students, [Types::StudentType], null: false

    def students
      Student.all
    end

    field :course_period_student, [Types::CoursePeriodStudentType], null: false do
      argument :course_id, ID, required: true
      argument :period_id, ID, required: true
    end

    def course_period_student(course_id:, period_id:)
      CoursePeriodStudent.where(course_id: course_id, period_id: period_id)
    end
  end
end
