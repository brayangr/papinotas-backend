# frozen_string_literal: true

# == Schema Information
#
# Table name: courses
#
#  id         :bigint           not null, primary key
#  full_name  :string
#  short_name :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Course < ApplicationRecord
  validates :full_name, presence: true, uniqueness: true
  validates :short_name, presence: true, uniqueness: true

  has_many :course_period_students,
           foreign_key: 'course_id',
           class_name: 'CoursePeriodStudent'
end
