# frozen_string_literal:true

module Types
  class PeriodType < Types::BaseObject
    field :id, ID, null: false
    field :name, String, null: false
    field :start_date, String, null: false
    field :end_date, String, null: false
    field :created_at, GraphQL::Types::ISO8601DateTime, null: false
    field :updated_at, GraphQL::Types::ISO8601DateTime, null: false
  end
end
