# frozen_string_literal: true

class Mutations::DeleteStudent < Mutations::BaseMutation
  argument :id, ID, required: true

  field :student, Types::StudentType, null: true
  field :errors, [String], null: false

  def resolve(id:)
    student = Student.where(id: id).first

    if !student.nil? && student.courses.empty? && student.delete
      { student: student, errors: [] }
    elsif student.nil?
      { student: nil, errors: ["student with id: #{id} not found"]}
    else
      { student: nil, errors: ["student with id: #{id} can't be deleted"] }
    end
  end
end
