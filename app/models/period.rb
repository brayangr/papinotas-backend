# frozen_string_literal: true

# == Schema Information
#
# Table name: periods
#
#  id         :bigint           not null, primary key
#  name       :string
#  start_date :date
#  end_date   :date
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
class Period < ApplicationRecord
  validates_uniqueness_of :name
  validates :name, presence: true
  validates :start_date, presence: true
  validates :end_date, presence: true

  has_many :course_period_students,
           foreign_key: 'course_id',
           class_name: 'CoursePeriodStudent'

  scope :active, lambda {
    where(
      Period.arel_table[:start_date].lteq(Date.today).and(
        Period.arel_table[:end_date].gteq(Date.today)
      )
    )
  }
end
