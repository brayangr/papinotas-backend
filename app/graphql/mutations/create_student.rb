# frozen_string_literal: true

class Mutations::CreateStudent < Mutations::BaseMutation
  argument :name, String, required: true
  argument :paternal_surname, String, required: true
  argument :maternal_surname, String, required: true
  argument :rut, String, required: true

  field :student, Types::StudentType, null: true
  field :errors, [String], null: false

  def resolve(name:, paternal_surname:, maternal_surname:, rut:)
    student = Student.new(
      name: name,
      paternal_surname: paternal_surname,
      maternal_surname: maternal_surname,
      rut: rut
    )

    if student.save
      { student: student, errors: [] }
    else
      { student: nil, errors: student.errors.full_messages }
    end
  end
end
