# frozen_string_literal: true

class Mutations::CreateCoursePeriodStudent < Mutations::BaseMutation
  argument :course, ID, required: true
  argument :period, ID, required: true
  argument :student, ID, required: true
  argument :list_number, Integer, required: true

  field :course_period_student, Types::CoursePeriodStudentType, null: true
  field :errors, [String], null: false

  def resolve(course:, period:, student:, list_number:)
    course_period_student = CoursePeriodStudent.create(
      course_id: course,
      period_id: period,
      student_id: student,
      list_number: list_number
    )

    if course_period_student.save
      { course_period_student: course_period_student, errors: [] }
    else
      {
        course_period_student: nil,
        errors: course_period_student.errors.full_messages
      }
    end
  end
end
