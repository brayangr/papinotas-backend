class CreateCoursePeriodStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :course_period_students do |t|
      t.references :course, foreign_key: true
      t.references :period, foreign_key: true
      t.references :student, foreign_key: true
      t.integer :list_number

      t.timestamps
    end

    add_index :course_period_students, %i[course_id period_id student_id], unique: true, name: 'course_period_student'
    add_index :course_period_students, %i[course_id list_number period_id], unique: true, name: 'course_list_number_period'
  end
end
