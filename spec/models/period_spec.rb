# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Period, type: :model do
  describe 'name field is unique' do
    let(:period) do
      Period.create(
        name: 'Periodo 1',
        start_date: DateTime.now,
        end_date: DateTime.now
      )
    end

    before do
      Period.create(
        name: 'Periodo 1',
        start_date: DateTime.now,
        end_date: DateTime.now
      )
      period
    end

    it 'is not created' do
      errors = period.errors.messages
      expect(errors[:name]).to eq(['has already been taken'])
    end
  end

  describe 'start_date and end_date field are required' do
    let(:period) { Period.create(name: 'Periodo 2') }

    before { period }

    it 'is is not created' do
      errors = period.errors.messages
      expect(errors[:start_date]).to eq(['can\'t be blank'])
      expect(errors[:end_date]).to eq(['can\'t be blank'])
    end
  end

  describe '.active' do
    let(:active_period) do
      Period.create(
        name: 'Periodo 1',
        start_date: 1.month.ago,
        end_date: 1.month.from_now
      )
    end

    let(:inactive_period) do
      Period.create(
        name: 'Periodo 2',
        start_date: 2.month.ago,
        end_date: 1.month.ago
      )
    end

    before do
      active_period
      inactive_period
    end

    it 'return only active periods' do
      expect(Period.active).to eq([active_period])
    end
  end
end
