# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Student, type: :model do
  describe 'need all the fields to be created' do
    let(:created_student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: Run.generate
      )
    end

    let(:not_created_student) { Student.create }

    before do
      created_student
      not_created_student
    end

    it 'is created' do
      expect(created_student.id).not_to eq(nil)
    end

    it 'is not created' do
      errors = not_created_student.errors.messages
      expect(errors[:name]).to eq(['can\'t be blank'])
      expect(errors[:paternal_surname]).to eq(['can\'t be blank'])
      expect(errors[:maternal_surname]).to eq(['can\'t be blank'])
      expect(errors[:rut]).to eq(['can\'t be blank', 'must be a valid rut'])
    end
  end

  describe 'rut field should be a valid rut' do
    let(:student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: '19420917-1'
      )
    end

    before do
      student
    end

    it 'should fails' do
      expect(student.errors.messages[:rut]).to eq(['must be a valid rut'])
    end
  end

  describe 'rut field should be unique' do
    let(:student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: '19420917-7'
      )
    end

    before do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: '19420917-7'
      )

      student
    end

    it 'is not created' do
      error = student.errors.messages[:rut]
      expect(error).to eq(['has already been taken'])
    end
  end
end
