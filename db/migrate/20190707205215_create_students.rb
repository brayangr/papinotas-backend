class CreateStudents < ActiveRecord::Migration[5.2]
  def change
    create_table :students do |t|
      t.string :name
      t.string :paternal_surname
      t.string :maternal_surname
      t.string :rut

      t.index :rut, unique: true

      t.timestamps
    end
  end
end
