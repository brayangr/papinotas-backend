# frozen_string_literal: true

require 'rails_helper'

RSpec.describe CoursePeriodStudent, type: :model do
  describe 'need all the fields to be created' do
    let(:course) do
      Course.create(full_name: 'Primero A', short_name: '1°A')
    end

    let(:period) do
      Period.create(
        name: 'Periodo 1',
        start_date: 1.month.ago,
        end_date: 1.month.from_now
      )
    end

    let(:student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: Run.generate
      )
    end

    let(:created) do
      CoursePeriodStudent.create(
        course: course,
        period: period,
        student: student,
        list_number: 1
      )
    end

    let(:not_created) { CoursePeriodStudent.create }

    before do
      course
      period
      student
      created
      not_created
    end

    it 'is created' do
      expect(created.id).not_to eq(nil)
    end

    it 'is not created' do
      errors = not_created.errors.messages

      expect(errors[:course]).to eq(['must exist'])
      expect(errors[:period]).to eq(['must exist'])
      expect(errors[:student]).to eq(['must exist'])
      expect(errors[:list_number]).to eq(['can\'t be blank', 'is not a number'])
    end
  end

  describe 'course, student, period coposition is unique' do
    let(:course) do
      Course.create(full_name: 'Primero A', short_name: '1°A')
    end

    let(:period) do
      Period.create(
        name: 'Periodo 1',
        start_date: 1.month.ago,
        end_date: 1.month.from_now
      )
    end

    let(:student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: Run.generate
      )
    end

    let(:created) do
      CoursePeriodStudent.create(
        course: course,
        period: period,
        student: student,
        list_number: 1
      )
    end

    let(:not_created) do
      CoursePeriodStudent.create(
        course: course,
        period: period,
        student: student,
        list_number: 1
      )
    end

    before do
      course
      period
      student
      created
      not_created
    end

    it 'is not created' do
      errors = not_created.errors.messages
      expect(errors[:list_number]).to eq(['has already been taken'])
      expect(errors[:student]).to eq(['has already been taken'])
    end
  end

  describe 'list_number field can only be integer' do
    let(:course) do
      Course.create(full_name: 'Primero A', short_name: '1°A')
    end

    let(:period) do
      Period.create(
        name: 'Periodo 1',
        start_date: 1.month.ago,
        end_date: 1.month.from_now
      )
    end

    let(:first_student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: Run.generate
      )
    end

    let(:second_student) do
      Student.create(
        name: 'Nombre',
        paternal_surname: 'Apellido 1',
        maternal_surname: 'Apellido 2',
        rut: Run.generate
      )
    end

    let(:with_string) do
      CoursePeriodStudent.create(
        course: course,
        period: period,
        student: first_student,
        list_number: 'first'
      )
    end

    let(:with_float) do
      CoursePeriodStudent.create(
        course: course,
        period: period,
        student: second_student,
        list_number: 1.0
      )
    end

    before do
      course
      period
      first_student
      second_student
      with_string
      with_float
    end

    it 'is not created' do
      expect(with_string.errors.messages[:list_number]).to eq(['is not a number'])
      expect(with_float.errors.messages[:list_number]).to eq(['must be an integer'])
    end
  end
end
