# frozen_string_literal: true

class Mutations::DeleteCourse < Mutations::BaseMutation
  argument :id, ID, required: true

  field :course, Types::CourseType, null: true
  field :errors, [String], null: false

  def resolve(id:)
    course = Course.where(id: id).first

    if !course.nil? && course.course_period_students.empty? && course.delete
      { course: course, errors: [] }
    elsif course.nil?
      { course: nil, errors: ["course with id: #{id} not found"]}
    else
      { course: nil, errors: ["course with id: #{id} can't be deleted"] }
    end
  end
end
