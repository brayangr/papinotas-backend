# frozen_string_literal: true

module Types
  class MutationType < Types::BaseObject
    field :create_course, mutation: Mutations::CreateCourse
    field :create_period, mutation: Mutations::CreatePeriod
    field :create_student, mutation: Mutations::CreateStudent
    field :create_course_period_student, mutation: Mutations::CreateCoursePeriodStudent
    field :create_courses_xlsx, mutation: Mutations::CreateCoursesXlsx
    field :create_periods_xlsx, mutation: Mutations::CreatePeriodsXlsx
    field :create_students_xlsx, mutation: Mutations::CreateStudentsXlsx
    field :delete_course, mutation: Mutations::DeleteCourse
    field :delete_period, mutation: Mutations::DeletePeriod
    field :delete_student, mutation: Mutations::DeleteStudent
  end
end
