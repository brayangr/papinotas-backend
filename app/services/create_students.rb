# frozen_string_literal: true

class CreateStudents
  require 'roo'

  def initialize(xlsx_data)
    @xlsx_route = 'students.xlsx'
    decode_base64_content = Base64.decode64(xlsx_data)
    File.open(@xlsx_route, 'wb') do |f|
      f.write(decode_base64_content)
    end
  end

  def load_data
    xlsx = Roo::Spreadsheet.open(@xlsx_route)

    ActiveRecord::Base.transaction do
      xlsx.each(
        name: 'Nombre',
        paternal_surname: 'Apellido Paterno',
        maternal_surname: 'Apellido Materno',
        rut: 'Rut',
        course: 'Curso',
        period: 'Periodo',
        list_number: 'Número de lista'
      ) do |hash|
        next if hash[:list_number] == 'Número de lista'

        student = Student.new(
          name: hash[:name],
          paternal_surname: hash[:paternal_surname],
          maternal_surname: hash[:maternal_surname],
          rut: hash[:rut]
        )

        if student.save
          course = Course.where(short_name: hash[:course]).first
          period = Period.where(name: hash[:period]).first
          list_number = hash[:list_number].to_i

          course_period_student = CoursePeriodStudent.new(
            course: course,
            period: period,
            student: student,
            list_number: list_number
          )

          unless course_period_student.save
            message = {
              error: course_period_student.errors.full_messages,
              id: hash[:full_name]
            }

            ActionCable.server.broadcast 'messages_channel', message

            return
          end
        else
          message = { error: student.errors.full_messages, id: hash[:rut] }
          ActionCable.server.broadcast 'messages_channel', message

          return
        end
      end
    end

    message = { success: 'Datos cargados correctamente' }
    ActionCable.server.broadcast 'messages_channel', message

    File.delete(@xlsx_route) if File.exist?(@xlsx_route)
  end
end
