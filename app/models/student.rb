# frozen_string_literal: true

# == Schema Information
#
# Table name: students
#
#  id               :bigint           not null, primary key
#  name             :string
#  paternal_surname :string
#  maternal_surname :string
#  rut              :string
#  created_at       :datetime         not null
#  updated_at       :datetime         not null
#
class Student < ApplicationRecord
  include RunCl::ActAsRun
  validates  :name, presence: true
  validates  :paternal_surname, presence: true
  validates  :maternal_surname, presence: true
  validates  :rut, presence: true, uniqueness: true
  has_run_cl :rut

  has_many :courses, foreign_key: 'student_id', class_name: 'CoursePeriodStudent'

  def rut
    self[:rut] = Run.format(self[:rut])
  end
end
