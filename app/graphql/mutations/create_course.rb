# frozen_string_literal: true

class Mutations::CreateCourse < Mutations::BaseMutation
  argument :full_name, String, required: true
  argument :short_name, String, required: true

  field :course, Types::CourseType, null: true
  field :errors, [String], null: false

  def resolve(full_name:, short_name:)
    course = Course.new(full_name: full_name, short_name: short_name)

    if course.save
      { course: course, errors: [] }
    else
      { course: nil, errors: course.errors.full_messages }
    end
  end
end
