# frozen_string_literal: true

class CreatePeriods
  require 'roo'

  def initialize(xlsx_data)
    @xlsx_route = 'periods.xlsx'
    decode_base64_content = Base64.decode64(xlsx_data)
    File.open(@xlsx_route, 'wb') do |f|
      f.write(decode_base64_content)
    end
  end

  def load_data
    xlsx = Roo::Spreadsheet.open(@xlsx_route)

    ActiveRecord::Base.transaction do
      xlsx.each(
        name: 'Nombre',
        start_date: 'Fecha de inicio',
        end_date: 'Fecha de término'
      ) do |hash|
        next if hash[:name] == 'Nombre' && hash[:start_date] == 'Fecha de inicio'

        period = Period.new(
          name: hash[:name],
          start_date: hash[:start_date],
          end_date: hash[:end_date]
        )

        next if period.save

        message = { error: period.errors.full_messages, id: hash[:name] }
        ActionCable.server.broadcast 'messages_channel', message

        return
      end

      message = { success: 'Datos cargados correctamente' }
      ActionCable.server.broadcast 'messages_channel', message
    end

    File.delete(@xlsx_route) if File.exist?(@xlsx_route)
  end
end
