# frozen_string_literal: true

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins '*'
    resource '*',
             headers: :any,
             methods: %i[get post put patch delete options head]
  end
end

Rails.application.config.action_cable.allowed_request_origins = ['http://0.0.0.0:3001']
