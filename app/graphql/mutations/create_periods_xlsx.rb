# frozen_string_literal: true

class Mutations::CreatePeriodsXlsx < Mutations::BaseMutation
  argument :file, ApolloUploadServer::Upload, required: false

  field :errors, [String], null: false

  def resolve(file:)
    PeriodsWorker.perform_async(Base64.encode64(File.read(file.tempfile)))
    { errors: [] }
  end
end
