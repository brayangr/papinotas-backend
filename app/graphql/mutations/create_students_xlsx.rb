# frozen_string_literal: true

class Mutations::CreateStudentsXlsx < Mutations::BaseMutation
  argument :file, ApolloUploadServer::Upload, required: false

  field :errors, [String], null: false

  def resolve(file:)
    StudentsWorker.perform_async(Base64.encode64(File.read(file.tempfile)))
    { errors: [] }
  end
end
