# frozen_string_literal: true

Rails.application.routes.draw do
  if Rails.env.development?
    mount GraphiQL::Rails::Engine, at: '/graphiql', graphql_path: '/graphql'
  end
  post '/graphql', to: 'graphql#execute'

  mount ActionCable.server => '/cable'

  namespace :api do
    namespace :v1 do
      resources :students
    end
  end
end
