# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

course = Course.create(full_name: 'Primero A', short_name: '1°A')

period = Period.create(
  name: 'Periodo 2019',
  start_date: 1.month.ago,
  end_date: 1.month.from_now
)

student1 = Student.create(
  name: 'Estudiante 1',
  paternal_surname: 'Apellido paterno 1',
  maternal_surname: 'Apellido materno 1',
  rut: Run.generate
)

student2 = Student.create(
  name: 'Estudiante 2',
  paternal_surname: 'Apellido paterno 2',
  maternal_surname: 'Apellido materno 2',
  rut: Run.generate
)

CoursePeriodStudent.create(
  course: course,
  period: period,
  student: student1,
  list_number: 1
)

CoursePeriodStudent.create(
  course: course,
  period: period,
  student: student2,
  list_number: 2
)
