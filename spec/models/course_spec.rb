# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Course, type: :model do
  describe 'can be created only if all the fields are provided' do
    let(:created_course) do
      Course.create(full_name: 'Primero A', short_name: '1°A')
    end

    let(:not_created_course) do
      Course.create(full_name: 'Primero B')
    end

    before do
      created_course
      not_created_course
    end

    it 'is created' do
      expect(created_course.id).not_to be_nil
    end

    it 'is not created' do
      expect(not_created_course.id).to be_nil
    end
  end

  describe 'full_name and short_name are uniques' do
    let(:not_created_course) do
      Course.create(full_name: 'Primero A', short_name: '1°A')
    end

    before do
      Course.create(full_name: 'Primero A', short_name: '1°A')
      not_created_course
    end

    it 'is not created' do
      errors = not_created_course.errors.messages
      expect(errors[:full_name]).to eq(['has already been taken'])
      expect(errors[:short_name]).to eq(['has already been taken'])
    end
  end
end
